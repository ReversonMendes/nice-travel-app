import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/modules/meu_cronograma/interfaces/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/novo_cronograma/novo_cronograma_module.dart';

import 'meu_cronograma_controller.dart';
import 'meu_cronograma_page.dart';
import 'meu_cronograma_repository.dart';

class MeuCronogramaModule extends ChildModule {

  static const NOVO_CRONOGRAMA_PATH = '/novo-cronograma';

  @override
  List<Bind> get binds => [
        Bind<IMeuCronogramaRepository>((_) => MeuCronogramaRepository()),
        $MeuCronogramaController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => MeuCronogramaPage()),
        ModularRouter(NOVO_CRONOGRAMA_PATH, module: NovoCronogramaModule()),
      ];

  static Inject get to => Inject<MeuCronogramaModule>.of();
}
