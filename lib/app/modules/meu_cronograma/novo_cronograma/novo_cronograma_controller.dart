import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/modules/meu_cronograma/interfaces/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';

part 'novo_cronograma_controller.g.dart';

@Injectable()
class NovoCronogramaController = _NovoCronogramaControllerBase
    with _$NovoCronogramaController;

abstract class _NovoCronogramaControllerBase with Store {

  IMeuCronogramaRepository _repository = MeuCronogramaModule.to.get<IMeuCronogramaRepository>();

  MeuCronograma meuCronograma;

  Future<void> saveCronograma(){
    return _repository.save(meuCronograma);
  }

  Future<void> removeCronograma(){
    return _repository.remove(meuCronograma);
  }

  updateCity(String placeId, String nomeLugar){
    meuCronograma.setNomeLugar(nomeLugar);
    meuCronograma.setPlaceId(placeId);
  }
}
