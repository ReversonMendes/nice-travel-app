import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';

part 'cronograma.g.dart';

class Cronograma = CronogramaBase with _$Cronograma;

abstract class CronogramaBase with Store {
  final int scheduleCod;
  final List<String> imagesUrl;

  final String userName;
  final String userUID;

  int qtdDays;
  String cityAddress;

  int numberStar = 0;
  bool isPublish = false;

  CronogramaBase(
      {@required this.scheduleCod,
      @required this.qtdDays,
      this.imagesUrl = const ['https://maps.googleapis.com/maps/api/place/photo?maxwidth=1125&photoreference=CmRaAAAACRqL_kaHKGeqAxnv-F37itCFZWx5C7yG1rp5qBNsDi0LjjLvwB87MxAXdsvLrt5sbbZ-wob1fe6ompONrtm1R8gatOsKg3_w35bThbjY68H_7LHxJ-nUiaVy9w6TNuBxEhAxvH1QViqK4V7fwvziSeYUGhRbc0nLiD2-yOopfms28l_RDp71Kw&key=AIzaSyASiKvQ4Ph4yIqF9VH8A2v9Fm-oP_T8NLQ	'],
      @required this.cityAddress,
      @required this.userName,
      @required this.userUID});

  get firstImage => imagesUrl != null ? imagesUrl.first : '';

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CronogramaBase &&
          runtimeType == other.runtimeType &&
          scheduleCod == other.scheduleCod;

  @override
  int get hashCode => scheduleCod.hashCode;
}
