import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/util/format_util.dart';

void main() {
  initModule(AtividadesModule());
  AtividadesController controller;

  setUp(() {
    controller = AtividadesModule.to.get<AtividadesController>();
  });

  group('AtividadesController Test', () {

    setUp(() {
      controller.atividades.clear();
    });

    test("First Test", () {
      expect(controller, isInstanceOf<AtividadesController>());
    });

    test("Test Delete Local Atividade", () {
      controller.atividades.add(Atividade(
          id: 1,
          nameOfPlace: 'Praia do Farol da Barra',
          startActivityDate: formatStringToHora('08:00'),
          finishActivityDate: formatStringToHora('13:00'),
          price: 25.0,
          styleActivity: 'SWIMMING'));
      expect(controller.atividades.length, 1);
      var atividade = Atividade();
      atividade.id = 1;
      controller.deleteLocal(atividade);
      expect(controller.atividades.length, 0);
    });

    test("Test Save Local Atividade", () {
      controller.atividades.add(Atividade(
          id: 1,
          nameOfPlace: 'Praia do Farol da Barra',
          startActivityDate: formatStringToHora('08:00'),
          finishActivityDate: formatStringToHora('13:00'),
          price: 25.0,
          styleActivity: 'SWIMMING'));
      expect(controller.atividades.length, 1);
      var atividade = Atividade();
      controller.saveLocal(atividade, 2);
      expect(controller.atividades.length, 2);
    });

    test("Test Update Local Atividade", () {
      var atividade = Atividade(
          id: 1,
          nameOfPlace: 'Praia do Farol da Barra',
          startActivityDate: formatStringToHora('08:00'),
          finishActivityDate: formatStringToHora('13:00'),
          price: 25.0,
          styleActivity: 'SWIMMING');
      controller.atividades.add(atividade);
      expect(controller.atividades.length, 1);
      atividade.price = 50.0;
      controller.saveLocal(atividade, null);
      expect(controller.atividades[0].price, 50.0);
    });
  });
}
