import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/modules/meu_cronograma/interfaces/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/novo_cronograma/novo_cronograma_controller.dart';
import 'package:nice_travel/app/modules/meu_cronograma/novo_cronograma/novo_cronograma_module.dart';

class MeuCronogramaRepositoryMock extends Mock
    implements IMeuCronogramaRepository {}

void main() {
  initModules([
    MeuCronogramaModule(),
    NovoCronogramaModule()
  ], changeBinds: [
    Bind<IMeuCronogramaRepository>((_) => MeuCronogramaRepositoryMock())
  ]);
  NovoCronogramaController novoCronogramaController;
  IMeuCronogramaRepository cronogramaRepository;
  setUp(() {
    novoCronogramaController =
        NovoCronogramaModule.to.get<NovoCronogramaController>();
    cronogramaRepository =
        MeuCronogramaModule.to.get<IMeuCronogramaRepository>();
  });

  group('NovoCronogramaController Test', () {
    test("Test Crontroller instance", () {
      expect(
          novoCronogramaController, isInstanceOf<NovoCronogramaController>());
    });

    test("Test Repository instance", () {
      expect(
          novoCronogramaController, isInstanceOf<NovoCronogramaController>());
    });

    test("Save Cronograma", () {
      expect(cronogramaRepository, isInstanceOf<MeuCronogramaRepositoryMock>());

      novoCronogramaController.saveCronograma();
      verify(cronogramaRepository.save(novoCronogramaController.meuCronograma));
    });
  });
}
