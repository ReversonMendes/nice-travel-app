import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/interfaces/atividade_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

part 'atividades_controller.g.dart';

@Injectable()
class AtividadesController = _AtividadesControllerBase
    with _$AtividadesController;

abstract class _AtividadesControllerBase with Store {
  IAtividadeRepository _repository =
      AtividadesModule.to.get<IAtividadeRepository>();

  @observable
  List<Atividade> atividades = ObservableList<Atividade>();

  void fillAtividades(List<Atividade> atividades) {
    this.atividades = atividades.asObservable();
  }

  Future<ApiResponse<List<Atividade>>> findAll(int scheduleCod) {
    return _repository.findAll(scheduleCod);
  }

  Future<void> deleteAtividade(Atividade atividade) {
    return _repository.delete(atividade);
  }

  void deleteLocal(Atividade atividade) {
    atividades.remove(atividade);
  }

  Future<int> saveAtividade(Atividade atividade) {
    return _repository.saveAtividade(atividade);
  }

  // @action
  void saveLocal(Atividade atividade, int id) {
    if (atividade.id == null) {
      atividade.id = id;
      atividades.add(atividade);
    } else {
      var index = atividades
          .asMap()
          .entries
          .where((element) => element.value.id == atividade.id)
          .first
          .key;
      atividades.insert(index, atividade);
      atividades.removeAt(index + 1);
    }
  }
}
