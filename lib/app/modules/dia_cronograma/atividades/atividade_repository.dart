import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/util/format_util.dart';

import 'interfaces/atividade_repository_interface.dart';

class AtividadeRepository implements IAtividadeRepository {
  List<Atividade> atividades = [
    Atividade(
        id: 1,
        nameOfPlace: 'Praia do Farol da Barra',
        startActivityDate: formatStringToHora('08:00'),
        finishActivityDate: formatStringToHora('13:00'),
        price: 25.0,
        styleActivity: 'SWIMMING'),
    Atividade(
        id: 2,
        nameOfPlace: 'Almoço no Pirão Baiano',
        startActivityDate: formatStringToHora('13:00'),
        finishActivityDate: formatStringToHora('15:00'),
        price: 45.0,
        styleActivity: 'RESTAURANT'),
    Atividade(
        id: 3,
        nameOfPlace: 'Farol da Barra',
        startActivityDate: formatStringToHora('15:00'),
        finishActivityDate: formatStringToHora('18:00'),
        price: 0.0,
        styleActivity: 'HISTORICAL MONUMENT'),
    Atividade(
        id: 4,
        nameOfPlace: 'Livre',
        startActivityDate: formatStringToHora('18:00'),
        finishActivityDate: formatStringToHora('20:00'),
        price: 0.0,
        styleActivity: 'OTHER'),
    Atividade(
        id: 5,
        nameOfPlace: 'Boteco do França',
        startActivityDate: formatStringToHora('20:00'),
        finishActivityDate: formatStringToHora('23:00'),
        price: 60.0,
        styleActivity: 'BAR'),
  ];

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  Future<ApiResponse<List<Atividade>>> findAll(int scheduleDayId) {
    // TODO: implement findAll
    atividades.asMap().forEach((index, element) {
      element.idScheduleDay = scheduleDayId;
      element.id = index;
    });
    atividades
        .sort((a, b) => a.finishActivityDate.compareTo(b.finishActivityDate));
    return Future.value(ApiResponse.completed(atividades));
  }

  @override
  Future<void> delete(Atividade atividade) {
    // TODO: implement delete
    atividades.remove(atividade);
    return Future.delayed(Duration(milliseconds: 800));
  }

  @override
  Future<int> saveAtividade(Atividade atividade) {
    if (atividade.id == null) {
      // TODO: implement new atividade
      atividade.id = atividades.length + 1;
      atividades.add(atividade);
    } else {
      // TODO: implement update atividade
      atividades.remove(atividade);
      atividades.add(atividade);
    }
    return Future.delayed(Duration(milliseconds: 800));
  }
}
