
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';

void main() {
  initModule(DiaCronogramaModule());
  DiaCronogramaController controller;

  setUp(() {
    controller = DiaCronogramaModule.to.get<DiaCronogramaController>();
  });

  group('DiaCronogramaController Test', () {

    setUp(() {
      controller.diasCronograma.clear();
    });

    test("First Test", () {
      expect(controller, isInstanceOf<DiaCronogramaController>());
    });

    test("get values", () {
      controller.diasCronograma.add(DiaCronograma(
          day: 1,
          id: 1,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      expect(controller.diasCronograma.length, 1);
    });

    test("deleteScheduleDay Value", () {
      controller.diasCronograma.add(DiaCronograma(
          day: 1,
          id: 1,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      expect(controller.diasCronograma.length, 1);
      controller.deleteLocal(5);
      expect(controller.diasCronograma.length, 1);
      controller.deleteLocal(1);
      expect(controller.diasCronograma.length, 0);
    });

    test("deleteScheduleDay Value", () {
      controller.diasCronograma.add(DiaCronograma(
          day: 1,
          id: 1,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      controller.diasCronograma.add(DiaCronograma(
          day: 2,
          id: 2,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      expect(controller.diasCronograma[0].id, 1);

      controller.reorderDays(1,2);

      expect(controller.diasCronograma[0].id, 2);
    });
  });
}
