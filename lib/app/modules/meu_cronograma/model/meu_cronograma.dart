import 'package:mobx/mobx.dart';

part 'meu_cronograma.g.dart';

class MeuCronograma = MeuCronogramaModel with _$MeuCronograma;

abstract class MeuCronogramaModel with Store {
  @observable
  String _nomeLugar;

  String _placeID;

  int _qtdDias;

  int _cod;

  get getCod => _cod;

  get getPlaceId =>  _placeID;

  @action
  setNomeLugar(String value) {
    _nomeLugar = value;
  }

  setQtdDias(String value) {
    _qtdDias = int.parse(value);
  }

  setPlaceId(String placeID) {
    _placeID= placeID;
  }

  int get qtdDias => _qtdDias;

  String get nomeLugar => _nomeLugar;

  MeuCronogramaModel();

  MeuCronogramaModel.initValues(this._nomeLugar, this._qtdDias, this._cod);

}
