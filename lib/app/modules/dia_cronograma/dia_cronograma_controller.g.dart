// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dia_cronograma_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $DiaCronogramaController = BindInject(
  (i) => DiaCronogramaController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DiaCronogramaController on _DiaCronogramaControllerBase, Store {
  final _$diasCronogramaAtom =
      Atom(name: '_DiaCronogramaControllerBase.diasCronograma');

  @override
  List<DiaCronograma> get diasCronograma {
    _$diasCronogramaAtom.reportRead();
    return super.diasCronograma;
  }

  @override
  set diasCronograma(List<DiaCronograma> value) {
    _$diasCronogramaAtom.reportWrite(value, super.diasCronograma, () {
      super.diasCronograma = value;
    });
  }

  final _$_DiaCronogramaControllerBaseActionController =
      ActionController(name: '_DiaCronogramaControllerBase');

  @override
  void fillDiasCronogramas(List<DiaCronograma> diasCronograma) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.fillDiasCronogramas');
    try {
      return super.fillDiasCronogramas(diasCronograma);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addDiaCronograma(DiaCronograma diaCronograma) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.addDiaCronograma');
    try {
      return super.addDiaCronograma(diaCronograma);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
diasCronograma: ${diasCronograma}
    ''';
  }
}
