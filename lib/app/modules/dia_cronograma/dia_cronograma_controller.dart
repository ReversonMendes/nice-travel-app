import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/interfaces/dia_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

part 'dia_cronograma_controller.g.dart';

@Injectable()
class DiaCronogramaController = _DiaCronogramaControllerBase
    with _$DiaCronogramaController;

abstract class _DiaCronogramaControllerBase with Store {
  IDiaCronogramaRepository _repository =
      DiaCronogramaModule.to.get<IDiaCronogramaRepository>();

  @observable
  List<DiaCronograma> diasCronograma = ObservableList<DiaCronograma>();

  @action
  void fillDiasCronogramas(List<DiaCronograma> diasCronograma) {
    this.diasCronograma = diasCronograma.asObservable();
  }

  @action
  void addDiaCronograma(DiaCronograma diaCronograma) {
    this.diasCronograma.add(diaCronograma);
  }

  Future<ApiResponse<List<DiaCronograma>>> findAll(int scheduleCod) {
    return _repository.findAll(scheduleCod);
  }

  Future deleteScheduleDay(int scheduleDayCod) {
    return _repository.deleteScheduleDay(scheduleDayCod);
  }

  void deleteLocal(int scheduleDayCod) {
    diasCronograma.removeWhere((element) => element.id == scheduleDayCod);
  }

  DiaCronograma createNewDiaCronograma(int scheduleCod) {
    final day = this.diasCronograma.length + 1;
    var diaCronograma = DiaCronograma.newInstance(day);
    addDiaCronograma(diaCronograma);
    _repository.createDiaCronograma(scheduleCod, day).then((value) =>
        diasCronograma.firstWhere((element) => element.day == day).id = value);
    return diaCronograma;
  }

  Future<dynamic> reorderDays(int from, int to) async {
    int indexFrom;
    int indexTo;
    DiaCronograma cronogramaFrom;
    DiaCronograma cronogramaTo;
    diasCronograma.asMap().forEach((index, element) {
      if (element.id == from) {
        indexFrom = index;
        cronogramaFrom = element;
      }
      if (element.id == to) {
        indexTo = index;
        cronogramaTo = element;
      }
    });
    var diaCronogramaTo = cronogramaTo.day;
    cronogramaTo.day = cronogramaFrom.day;
    diasCronograma.insert(indexFrom, cronogramaTo);
    diasCronograma.removeAt(indexFrom + 1);
    cronogramaFrom.day = diaCronogramaTo;
    diasCronograma.insert(indexTo, cronogramaFrom);
    diasCronograma.removeAt(indexTo + 1);
    return _repository.reorderDays(from, to);
  }
}
