
import 'package:flutter/foundation.dart';

class SessionUser {
  final String displayName;
  final String email;
  final String uid;
  String tokenMessage;

  SessionUser({
    @required this.displayName,
    @required this.email,
    @required this.uid,
    @required this.tokenMessage});
}
