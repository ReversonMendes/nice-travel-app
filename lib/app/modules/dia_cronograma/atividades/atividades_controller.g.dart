// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'atividades_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $AtividadesController = BindInject(
  (i) => AtividadesController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AtividadesController on _AtividadesControllerBase, Store {
  final _$atividadesAtom = Atom(name: '_AtividadesControllerBase.atividades');

  @override
  List<Atividade> get atividades {
    _$atividadesAtom.reportRead();
    return super.atividades;
  }

  @override
  set atividades(List<Atividade> value) {
    _$atividadesAtom.reportWrite(value, super.atividades, () {
      super.atividades = value;
    });
  }

  @override
  String toString() {
    return '''
atividades: ${atividades}
    ''';
  }
}
