import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/shared/custom_box_shadow.dart';

class MeuCronogramaListItem extends StatelessWidget {
  final Cronograma cronograma;

  MeuCronogramaListItem({@required this.cronograma});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          margin: EdgeInsets.only(left: 8, right: 8, bottom: 6, top: 6),
          height: 180,
          decoration: _buildBoxShadow(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildCityAdress(),
                        buildCityInfo(context),
                      ]))
            ],
          )),
      onTap: () => _sendToMeuCronogramaPage(context),
    );
  }

  _sendToMeuCronogramaPage(BuildContext context) {
    return Navigator.of(context)
        .pushNamed(AppModule.DIA_CRONOGRAMA_PATH, arguments: cronograma);
  }

  BoxDecoration _buildBoxShadow() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(3.0),
        color: Colors.blueGrey,
        boxShadow: [buildBoxShadow(3.0)],
        image: buildDecorationImage());
  }

  Row buildCityInfo(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _buildPersonName(),
        _buildQtdDias(),
      ],
    );
  }

  Row _buildQtdDias() {
    return Row(
      children: <Widget>[
        Text(
          '${this.cronograma.qtdDays} Dia(s)',
          style: TextStyle(
              fontSize: 15, color: Colors.white, fontWeight: FontWeight.w900),
        ),
        Icon(
          Icons.today,
          color: Colors.white,
        ),
      ],
    );
  }

  Row _buildPersonName() {
    return Row(
      children: <Widget>[
        Icon(
          Icons.person,
          color: Colors.white,
        ),
        Text(
          this.cronograma.userName,
          style: TextStyle(
              fontSize: 15, color: Colors.white, fontWeight: FontWeight.w900),
        ),
      ],
    );
  }

  Text _buildCityAdress() {
    return Text(
      this.cronograma.cityAddress,
      style: TextStyle(
          fontSize: 30, color: Colors.white, fontWeight: FontWeight.w900),
    );
  }

  DecorationImage buildDecorationImage() {
    return DecorationImage(
        fit: BoxFit.cover,
        colorFilter: new ColorFilter.mode(
            Colors.white.withOpacity(0.8), BlendMode.dstATop),
        image: CachedNetworkImageProvider(this.cronograma.firstImage));
  }
}
