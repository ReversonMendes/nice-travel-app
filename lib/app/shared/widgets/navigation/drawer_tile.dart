import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {
  final IconData icon;
  final String text;
  final String navigationPath;

  DrawerTile(this.icon, this.text, this.navigationPath, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => navigateToSelectedItem(context),
        child: Container(
          height: 60.0,
          child: Row(
            children: <Widget>[
              Icon(icon,
                  size: 32.0,
                  color: ModalRoute.of(context).settings.name == navigationPath
                      ? Theme.of(context).primaryColor
                      : Colors.grey[700]),
              SizedBox(
                width: 32.0,
              ),
              Expanded(
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: 16.0,
                      color:
                          ModalRoute.of(context).settings.name == navigationPath
                              ? Theme.of(context).primaryColor
                              : Colors.grey[700]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void navigateToSelectedItem(BuildContext context) {
    Navigator.of(context).pop();
    Navigator.of(context).popAndPushNamed(navigationPath);
  }
}
