import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/interfaces/dia_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

class MockClient extends Mock implements Dio {}

void main() {
  IDiaCronogramaRepository repository;

  setUp(() {
    repository = DiaCronogramaRepository();
  });

  group('DiaCronogramaRepository Test', () {
     test("First Test", () {
       expect(repository, isInstanceOf<DiaCronogramaRepository>());
     });

     test('Test list Cronograma', () async {
       ApiResponse<List<DiaCronograma>> cronogramas = await repository.findAll(1);
       expect(cronogramas.data.length, 4);
     });

  });
}
