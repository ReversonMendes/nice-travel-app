import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_widget.dart';

import 'mock_json_google.dart';

class DioMock extends Mock implements DioForNative {}

void main() {

  FlavorConfig(
    name: "DEVELOP",
    variables: {
      "google_place_api": 'teste',
    },
  );

  initModule(AppModule(), changeBinds: [Bind<Dio>((_) => DioMock())]);

  Dio client;

  setUp(() {
    client = AppModule.to.get<Dio>();
  });

  testWidgets('CityAutocompleteWidget show list when search element',
      (tester) async {
    when(client.get(
            'https://maps.googleapis.com/maps/api/place/autocomplete/json',
            queryParameters: anyNamed('queryParameters')))
        .thenAnswer(
            (_) async => Response(data: jsonGooglePlaceMock, statusCode: 200));

    var cityAutocompleteController = CityAutocompleteController();
    await tester.pumpWidget(buildTestableWidget(Scaffold(
      body: CityAutocompleteWidget(
        inputDecoration: InputDecoration(),
        onSelectCity: null,
        controller: cityAutocompleteController,
      ),
    )));
    final textAutocompleteFinder = find.byType(TextField);
    expect(textAutocompleteFinder, findsOneWidget);
    await tester.tap(textAutocompleteFinder);
    await tester.enterText(textAutocompleteFinder, 'Salvador');
    await tester.pump();

    final autocompleteFinder = find.byType(ListTile);
    expect(autocompleteFinder, findsNWidgets(5));
  });
}
