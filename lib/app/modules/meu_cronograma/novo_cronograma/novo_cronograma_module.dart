import 'package:flutter_modular/flutter_modular.dart';

import 'novo_cronograma_controller.dart';
import 'novo_cronograma_page.dart';

class NovoCronogramaModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $NovoCronogramaController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => NovoCronogramaPage(
                  cronograma: args.data,
                )),
      ];

  static Inject get to => Inject<NovoCronogramaModule>.of();
}
