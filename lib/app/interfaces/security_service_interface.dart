import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/session_user.dart';

abstract class ISecurityService implements Disposable {

  SessionUser getSessionUser();
  bool isLoggedIn();
  void signIn();
  void signOut();
}
