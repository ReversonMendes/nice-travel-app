import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class AddButton extends StatelessWidget {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();

  final SessionUser sessionUser;
  final Cronograma cronograma;

  AddButton({Key key, @required this.sessionUser, @required this.cronograma})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        child: Column(
          children: <Widget>[
            Icon(
              Icons.add_circle,
              size: 40.0,
              color: Colors.blue,
            ),
            textButton("Novo dia"),
          ],
        ),
        onPressed: () => validateLoginAction(
            context: context,
            user: sessionUser,
            cronograma: cronograma,
            duplicationAction: () => _duplicate(context, cronograma),
            successAction: () => sendToAtividadePageWithNovoDia(context)));
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        textAlign: TextAlign.center,
      );

  void _duplicate(BuildContext context, Cronograma cronograma) {
    showCircularProgress(context);
    //Todo duplicar schedule
  }

  Future sendToAtividadePageWithNovoDia(BuildContext context) async {
    DiaCronograma diaCronograma =
        _controller.createNewDiaCronograma(cronograma.scheduleCod);

    Map<String, dynamic> parameters = {
      'diaCronograma': diaCronograma,
      'cronograma': cronograma
    };

    Navigator.of(context).pushNamed(
        AppModule.DIA_CRONOGRAMA_PATH + DiaCronogramaModule.ATIVIDADES_PATH,
        arguments: parameters);
  }
}
