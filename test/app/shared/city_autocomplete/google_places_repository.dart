import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/shared/city_autocomplete/google_places_repository.dart';
import 'package:nice_travel/app/shared/city_autocomplete/model/places_model.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'mock_json_google.dart';

class MockClient extends Mock implements DioForNative {}

void main() {
  ICityAutocomplete repository;
  MockClient client;

  setUp(() {
    client = MockClient();
    repository = GooglePlacesAPI(client);
  });

  group('CityAutocompleteRepository Test', () {
    test("First Test", () {
      expect(repository, isInstanceOf<GooglePlacesAPI>());
    });

    test('returns a Post if the http call completes successfully', () async {
      when(client.get(
              'https://maps.googleapis.com/maps/api/place/autocomplete/json',
              queryParameters: anyNamed('queryParameters')))
          .thenAnswer((_) async =>
              Response(data: jsonGooglePlaceMock, statusCode: 200));
      List<GooglePlacesModel> data = await repository.getCities('Salvador');

      expect(data.length, 5);
    });
  });
}
