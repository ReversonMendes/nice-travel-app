import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';

class DeleteButton extends StatelessWidget {
  final AtividadesController _controller =
      AtividadesModule.to.get<AtividadesController>();

  final Atividade atividade;

  DeleteButton({Key key, @required this.atividade}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: MaterialButton(
        key: Key('deleteatividade_button'),
        height: 45,
        //Wrap with Material
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
        elevation: 18.0,
        color: Color(0xFF801E08),
        clipBehavior: Clip.antiAlias,
        // Add This
        child: new Text('Deletar',
            style: new TextStyle(fontSize: 16.0, color: Colors.white)),
        onPressed: () {
          removeDialog(context, "Deseja remover essa atividade?",
              () => deleteAction(context));
        },
      ),
    );
  }

  deleteAction(BuildContext context) {
    Navigator.of(context).pop(); //Modal
    showCircularProgress(context);
    _controller.deleteAtividade(atividade).then((value) {
      Navigator.of(context).pop(); //Progress
      Navigator.of(context).pop(); //Page
      _controller.deleteLocal(atividade);
    });
  }
}
