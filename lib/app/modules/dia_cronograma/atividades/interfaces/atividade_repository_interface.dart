import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/shared/api_response.dart';

abstract class IAtividadeRepository implements Disposable {

  Future<ApiResponse<List<Atividade>>> findAll(int scheduleDayId);

  Future<void> delete(Atividade atividade);

  Future<int> saveAtividade(Atividade atividade);

}
