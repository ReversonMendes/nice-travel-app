import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';

abstract class IDiaCronogramaRepository implements Disposable {

  Future<ApiResponse<List<DiaCronograma>>> findAll(int scheduleCod);

  Future<void> reorderDays(int from, int to);

  Future<void> deleteScheduleDay(int scheduleDayCod);

  Future<int> createDiaCronograma(int scheduleCod, int dia);
}
