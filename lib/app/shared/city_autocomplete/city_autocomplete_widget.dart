import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/model/places_model.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';
import 'package:nice_travel/app/util/debouncer.dart';

class CityAutocompleteWidget extends StatefulWidget {
  final InputDecoration inputDecoration;
  final CityAutocompleteController controller;
  final Function onSelectCity;

  const CityAutocompleteWidget(
      {Key key,
      @required this.inputDecoration,
      @required this.onSelectCity,
      @required this.controller})
      : super(key: key);

  @override
  _CityAutocompleteWidgetState createState() =>
      _CityAutocompleteWidgetState(inputDecoration, controller, onSelectCity);
}

class _CityAutocompleteWidgetState extends State<CityAutocompleteWidget> {
  final ICityAutocomplete _cityRepository =
      AppModule.to.get<ICityAutocomplete>();

  final InputDecoration inputDecoration;
  final CityAutocompleteController controller;
  final Function onSelectCity;
  final _debounce = Debounce(milliseconds: 500);

  bool _focusOn = false;

  _CityAutocompleteWidgetState(
      this.inputDecoration, this.controller, this.onSelectCity) {
    controller.field = AutoCompleteTextField<GooglePlacesModel>(
      decoration: inputDecoration,
      clearOnSubmit: false,
      onFocusChanged: (focus) =>
          clearOnFocusOutWhenValueWasNotSelected(focus, onSelectCity),
      itemBuilder: itemBuilder,
      itemFilter: (item, query) => _onItemFilter(item, query),
      itemSorter: (a, b) => _onSorter(a, b),
      itemSubmitted: (data) => _onSubmitted(data),
      textChanged: (text) async => _onTextChanged(text),
      key: GlobalKey<AutoCompleteTextFieldState<GooglePlacesModel>>(
          debugLabel: "autocomplete_google"),
      suggestions: [],
    );
  }

  bool _onItemFilter(GooglePlacesModel item, String query) {
    return item.terms[0].value.toLowerCase().startsWith(query.toLowerCase());
  }

  int _onSorter(GooglePlacesModel a, GooglePlacesModel b) {
    return a.terms[0].value.compareTo(b.terms[0].value);
  }

  void _onTextChanged(String text) {
    controller.cityInputsNull = true;
    _callCitySearch(text);
  }

  void _onSubmitted(GooglePlacesModel data) {
    controller.field.textField.controller.text = data.terms[0].value;
    controller.changePlaceId(data.placeId);
    controller.changeNameCity(controller.field.textField.controller.text);
    Function.apply(widget.onSelectCity, []);
    controller.cityInputsNull = false;
  }

  void _callCitySearch(String text) {
    if (text.length >= 3) {
      if (controller.field.suggestions.isEmpty) {
        _searchCitySuggestions(text);
      } else {
        _debounce.run(() {
          _searchCitySuggestions(text);
        });
      }
    }
  }

  void _searchCitySuggestions(String text) {
    _cityRepository.getCities(text).then((onValue) {
      if (controller.field != null && onValue != null) {
        controller.field.updateSuggestions(onValue);
      }
    });
  }

  clearOnFocusOutWhenValueWasNotSelected(bool focus, Function changeCity) {
    if (!focus && controller.cityInputsNull) {
      controller.field.clear();
      widget.controller.clearPlaceID();
      Function.apply(changeCity, []);
    }
    _focusOn = focus;
  }

  Widget itemBuilder(BuildContext context, GooglePlacesModel suggestion) {
    return Visibility(
      child: ListTile(
        key: Key('autocomplete_field_${suggestion.id}'),
        title: Text(suggestion.description),
        trailing: Text(suggestion.terms[1].value),
      ),
      visible: _focusOn,
    );
  }

  @override
  Widget build(BuildContext context) {
    return controller.field;
  }
}
