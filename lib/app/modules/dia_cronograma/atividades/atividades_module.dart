import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';

import 'atividade_repository.dart';
import 'atividades_controller.dart';
import 'atividades_page.dart';
import 'nova_atividade/nova_atividade_page.dart';

class AtividadesModule extends ChildModule {
  static const ATIVIDADE_PATH = '/atividade';
  static const ATIVIDADE_FULL_PATH = AppModule.DIA_CRONOGRAMA_PATH + DiaCronogramaModule.ATIVIDADES_PATH + ATIVIDADE_PATH;

  @override
  List<Bind> get binds => [
        Bind<AtividadeRepository>((i) => AtividadeRepository()),
        $AtividadesController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => AtividadesPage(
                cronograma: args.data['cronograma'],
                diaCronograma: args.data['diaCronograma'])),
        ModularRouter(ATIVIDADE_PATH,
            child: (_, args) => NovaAtividadePage(
                atividade: args.data['atividade'],
                cronograma: args.data['cronograma'],
                diaCronograma: args.data['diaCronograma'])),
      ];

  static Inject get to => Inject<AtividadesModule>.of();
}
