import 'package:flutter/material.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/error_connection_widget.dart';
import 'package:nice_travel/app/shared/widgets/loading_widget.dart';

typedef onCompletedList = void Function(List);


class APIList<ApiResponse> extends StatefulWidget {
  final Future<ApiResponse> elements;
  final onCompletedList onCompleted;
  final Widget listWidget;

  const APIList(
      {Key key,
        @required this.elements,
        @required this.onCompleted,
        @required this.listWidget})
      : super(key: key);

  @override
  _APIListState createState() => _APIListState();
}

class _APIListState extends State<APIList> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ApiResponse>(
        future: widget.elements,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Loading();
          } else {
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Loading(
                  loadingMessage: snapshot.data.message,
                );
              case Status.COMPLETED:
                widget.onCompleted(snapshot.data.data);
                return widget.listWidget;
              case Status.ERROR:
                return ErrorConnection(
                  errorMessage: snapshot.data.message,
                  onRetryPressed: () => setState(() {
                    /*Reload*/
                  }),
                );
                break;
            }
            return Container();
          }
        });
  }
}
