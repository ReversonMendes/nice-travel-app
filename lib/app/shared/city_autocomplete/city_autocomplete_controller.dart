import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/shared/city_autocomplete/model/places_model.dart';

part 'city_autocomplete_controller.g.dart';

class CityAutocompleteController = _CityAutocompleteControllerBase
    with _$CityAutocompleteController;

abstract class _CityAutocompleteControllerBase with Store {
  @observable
  String _placeId;

  @observable
  String _nameCity;

  bool cityInputsNull = true;

  AutoCompleteTextField<GooglePlacesModel> field;

  @action
  void changePlaceId(String placeId) {
    _placeId = placeId;
  }

  String getPlaceId() => _placeId;

  @action
  void changeNameCity(String nameCity) {
    _nameCity = nameCity;
  }

  String getNameCity() => _nameCity;

  @action
  clearPlaceID() {
    _placeId = null;
    _nameCity = null;
    if(field != null) {
      field.clear();
    }
  }
}
