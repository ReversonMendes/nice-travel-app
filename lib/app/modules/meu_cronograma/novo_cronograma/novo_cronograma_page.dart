import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_widget.dart';
import 'package:nice_travel/app/shared/loading_util.dart';

import 'novo_cronograma_controller.dart';

class NovoCronogramaPage extends StatefulWidget {
  final MeuCronograma cronograma;

  const NovoCronogramaPage({Key key, this.cronograma}) : super(key: key);

  @override
  _NovoCronogramaPageState createState() => _NovoCronogramaPageState();
}

class _NovoCronogramaPageState
    extends ModularState<NovoCronogramaPage, NovoCronogramaController> {
  GlobalKey<FormState> _formKey = GlobalKey();

  final autocompleteController = CityAutocompleteController();

  @override
  void initState() {
    controller.meuCronograma = widget.cronograma ?? MeuCronograma();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meu Cronograma'),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Wrap(runSpacing: 15.0, children: <Widget>[
                _buildAutocompleteCity(),
                _builInputField(
                    initalValue: controller.meuCronograma.qtdDias,
                    hintText: 'Quantidade de Dias',
                    onChanged: controller.meuCronograma.setQtdDias,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    keyboardType: TextInputType.number),
                _buildSaveButton(),
                _buildRemoveButton(),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAutocompleteCity() {
    return CityAutocompleteWidget(
        controller: autocompleteController,
        inputDecoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: "Vai para onde?",
            labelStyle: TextStyle(color: Colors.black),
            suffixIcon: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                autocompleteController.clearPlaceID();
                // autoCompleteField.clearPlaceID();
                controller.updateCity(autocompleteController.getPlaceId(),
                    autocompleteController.getNameCity());
              },
            )),
        onSelectCity: () => controller.updateCity(
            autocompleteController.getPlaceId(),
            autocompleteController.getNameCity()));
  }

  changeCity() {}

  Widget _buildSaveButton() {
    return MaterialButton(
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.save,
                  color: Colors.white,
                ),
                SizedBox(width: 10),
                Text(
                  'Salvar',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                  ),
                ),
              ]),
        ),
        onPressed: () => salvar());
  }

  Widget _buildRemoveButton() {
    return Visibility(
      visible: controller.meuCronograma.getCod != null,
      child: MaterialButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          color: Colors.red,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.delete_forever,
                    color: Colors.white,
                  ),
                  SizedBox(width: 10),
                  Text(
                    'Deletar',
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                    ),
                  ),
                ]),
          ),
          onPressed: () => remover()),
    );
  }

  Widget _builInputField(
      {@required initalValue,
      @required String hintText,
      @required Function onChanged,
      TextInputType keyboardType,
      int maxLength,
      List<TextInputFormatter> inputFormatters}) {
    return TextFormField(
      validator: RequiredValidator(errorText: 'Campo Obrigatório'),
      maxLength: maxLength,
      initialValue: initalValue?.toString(),
      onChanged: onChanged,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        labelText: hintText,
        border: InputBorder.none,
        focusedBorder: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(),
        focusedErrorBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
        errorBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      ),
    );
  }

  salvar() {
    if (_formKey.currentState.validate()) {
      LoadingUtil.showLoadingAlert(context);
      controller.saveCronograma().then((value) =>
          Navigator.of(context).popAndPushNamed(AppModule.MEUS_CRONOGRAMAS_PATH));
    }
  }

  remover() {
    LoadingUtil.showLoadingAlert(context);
    controller.removeCronograma().then((value) =>
        Navigator.of(context).popAndPushNamed(AppModule.MEUS_CRONOGRAMAS_PATH));
  }
}
