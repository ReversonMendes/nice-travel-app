import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/cronograma_list_item.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/error_connection_widget.dart';
import 'package:nice_travel/app/shared/widgets/loading_widget.dart';
import 'package:nice_travel/app/shared/widgets/navigation/navigation_drawer.dart';

import 'meu_cronograma_controller.dart';

class MeuCronogramaPage extends StatefulWidget {
  final String title;

  const MeuCronogramaPage({Key key, this.title = "Meus Cronogramas"})
      : super(key: key);

  @override
  _MeuCronogramaPageState createState() => _MeuCronogramaPageState();
}

class _MeuCronogramaPageState
    extends ModularState<MeuCronogramaPage, MeuCronogramaController> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder<ApiResponse<List<Cronograma>>>(
          future: controller.findAll(),
          builder: (ctx, meusCronogramas) {
            if (!meusCronogramas.hasData) {
              return Loading();
            } else {
              switch (meusCronogramas.data.status) {
                case Status.LOADING:
                  return Loading(
                    loadingMessage: meusCronogramas.data.message,
                  );
                case Status.COMPLETED:
                  return _buildListView(meusCronogramas.data);
                case Status.ERROR:
                  return ErrorConnection(
                    errorMessage: meusCronogramas.data.message,
                    onRetryPressed: () => controller.findAll(),
                  );
              }
              return Container();
            }
          }),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add), onPressed: () => _sendToNovoCronogramaPage()),
    );
  }

  ListView _buildListView(ApiResponse<List<Cronograma>> meusCronogramas) {
    return ListView.builder(
      itemCount: meusCronogramas.data.length,
      itemBuilder: (BuildContext context, int index) {
        var meuCronograma = meusCronogramas.data[index];
        return MeuCronogramaListItem(cronograma: meuCronograma);
      },
    );
  }

  _sendToNovoCronogramaPage() {
    Navigator.of(context)
        .pushNamed(ModalRoute.of(context).settings.name + MeuCronogramaModule.NOVO_CRONOGRAMA_PATH);
  }
}
