import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/button_bar_schedule_day.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_list.dart';

void main() {
  initModules([AppModule(), DiaCronogramaModule()]);

  testWidgets('DiaCronogramaPage has title', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '1');
    await tester.pumpWidget(
        buildTestableWidget(DiaCronogramaPage(cronograma: cronograma)));
    final dayScheduleFinder = find.byType(DayScheduleList);
    final buttonBarScheduleFinder = find.byType(ButtonBarScheduleDay);
    expect(dayScheduleFinder, findsOneWidget);
    expect(buttonBarScheduleFinder, findsOneWidget);
  });
}
