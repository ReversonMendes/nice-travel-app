import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'interfaces/security_service_interface.dart';

part 'security_service.g.dart';

@Injectable()
class SecurityService implements ISecurityService {

  SessionUser sessionUser = SessionUser(
      displayName: 'Thiago',
      email: 'thiago@gmail.com',
      tokenMessage: null,
      uid: "123");

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  SessionUser getSessionUser() {
    return sessionUser;
  }

  @override
  bool isLoggedIn() {
    return getSessionUser() != null;
  }

  @override
  void signIn() {
    // TODO: implement signIn
  }

  @override
  void signOut() {
    // TODO: implement signOut
  }
}
