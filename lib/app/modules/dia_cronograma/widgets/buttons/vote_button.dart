import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class VoteButton extends StatefulWidget {

  final SessionUser sessionUser;
  final Cronograma cronograma;

  const VoteButton(
      {Key key, @required this.sessionUser, @required this.cronograma})
      : super(key: key);

  @override
  _VoteButtonState createState() => _VoteButtonState();
}

class _VoteButtonState extends State<VoteButton> {
  bool hasLiked = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
        child: MaterialButton(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.star,
                  size: 40.0,
                  color: Colors.yellow,
                ),
                textButton("Curtir"),
              ],
            ),
            onPressed: () => validateLoginAction(
                context: context,
                user: widget.sessionUser,
                cronograma: widget.cronograma,
                duplicationAction: () => _duplicate(context, widget.cronograma),
                successAction: () => voteAction(context))),
        duration: Duration(seconds: 1),
        opacity: hasLiked ? 0.4 : 1.0);
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        textAlign: TextAlign.center,
      );

  void _duplicate(BuildContext context, Cronograma cronograma) {
    showCircularProgress(context);
    //Todo duplicar schedule
  }

  voteAction(BuildContext context) {
    //TODO implementar voto (garantir que o usuário esteja logado, e que o usuário não possa votar + de 1 vez)
  }
}
