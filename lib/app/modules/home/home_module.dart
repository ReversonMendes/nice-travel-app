import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';

import 'home_controller.dart';
import 'home_page.dart';

class HomeModule extends ChildModule {

  @override
  List<Bind> get binds => [
        $HomeController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(AppModule.VIAGEM_PATH, child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
