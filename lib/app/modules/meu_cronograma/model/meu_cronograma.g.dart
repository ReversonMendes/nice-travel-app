// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meu_cronograma.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MeuCronograma on MeuCronogramaModel, Store {
  final _$_nomeLugarAtom = Atom(name: 'MeuCronogramaModel._nomeLugar');

  @override
  String get _nomeLugar {
    _$_nomeLugarAtom.reportRead();
    return super._nomeLugar;
  }

  @override
  set _nomeLugar(String value) {
    _$_nomeLugarAtom.reportWrite(value, super._nomeLugar, () {
      super._nomeLugar = value;
    });
  }

  final _$MeuCronogramaModelActionController =
      ActionController(name: 'MeuCronogramaModel');

  @override
  dynamic setNomeLugar(String value) {
    final _$actionInfo = _$MeuCronogramaModelActionController.startAction(
        name: 'MeuCronogramaModel.setNomeLugar');
    try {
      return super.setNomeLugar(value);
    } finally {
      _$MeuCronogramaModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
