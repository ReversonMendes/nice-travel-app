import 'dart:async';

import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import 'interfaces/meu_cronograma_repository_interface.dart';

class MeuCronogramaRepository implements IMeuCronogramaRepository {
  List<Cronograma> _meusCronogramas = [
    Cronograma(
        cityAddress: 'Salvador',
        qtdDays: 2,
        scheduleCod: 1,
        userName: 'Thiago',
        userUID: '123')
  ];

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  Future<ApiResponse<List<Cronograma>>> findAll() {
    _meusCronogramas.sort((a, b) => a.scheduleCod.compareTo(b.scheduleCod));
    return Future.value(ApiResponse.completed(_meusCronogramas));
  }

  @override
  Future<void> remove(MeuCronograma meuCronograma) {
    _meusCronogramas.remove(Cronograma(scheduleCod: meuCronograma.getCod));
    return Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<void> save(MeuCronograma meuCronograma) {
    if (meuCronograma.getCod == null) {
      Cronograma cronograma = _criarCronograma(meuCronograma);
      _meusCronogramas.add(cronograma);
    } else {
      Cronograma cronograma = _atualizarCronograma(meuCronograma);
      _meusCronogramas.remove(cronograma);
      _meusCronogramas.add(cronograma);
    }
    return Future.value();
  }

  Cronograma _atualizarCronograma(MeuCronograma meuCronograma) {
     var cronograma = _meusCronogramas
        .where((element) => element.scheduleCod == meuCronograma.getCod)
        .first;
    cronograma.cityAddress = meuCronograma.nomeLugar;
    cronograma.qtdDays = meuCronograma.qtdDias;
    return cronograma;
  }

  Cronograma _criarCronograma(MeuCronograma meuCronograma) {
    var cronograma = Cronograma(
        cityAddress: meuCronograma.nomeLugar,
        qtdDays: meuCronograma.qtdDias,
        scheduleCod: meuCronograma.nomeLugar.length,
        userName: 'Thiago',
        userUID: '1');
    return cronograma;
  }
}
