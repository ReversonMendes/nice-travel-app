import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_page.dart';
import 'package:nice_travel/app/shared/widgets/navigation/navigation_drawer.dart';

void main() {
  initModules([AppModule(), MeuCronogramaModule()]);
  testWidgets('MeuCronogramaPage has title', (tester) async {
     await tester.pumpWidget(buildTestableWidget(MeuCronogramaPage(title: 'Meus Cronogramas')));
     final titleFinder = find.text('Meus Cronogramas');
     expect(titleFinder, findsOneWidget);
  });

  testWidgets('MeuCronogramaPage has drawer', (tester) async {
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaPage()));
    final drawerFinder = find.byIcon(Icons.menu);
    expect(drawerFinder, findsOneWidget);
    await tester.tap(drawerFinder);
    await tester.pump();
    final navigationDrawerFinder = find.byType(NavigationDrawer);
    expect(navigationDrawerFinder, findsOneWidget);
  });
}
