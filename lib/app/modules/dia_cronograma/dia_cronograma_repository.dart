import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import 'interfaces/dia_cronograma_repository_interface.dart';

class DiaCronogramaRepository implements IDiaCronogramaRepository {
  List<DiaCronograma> diasCronograma = [
    DiaCronograma(
        day: 1,
        id: 1,
        priceDay: 25,
        qtdActivities: 5,
        typeFirstActivity: 'Park'),
    DiaCronograma(
        day: 2,
        id: 2,
        priceDay: 50,
        qtdActivities: 3,
        typeFirstActivity: 'SWIMMING'),
    DiaCronograma(
        day: 3,
        id: 3,
        priceDay: 40,
        qtdActivities: 6,
        typeFirstActivity: 'SPORT'),
    DiaCronograma(
        day: 4,
        id: 4,
        priceDay: 140,
        qtdActivities: 2,
        typeFirstActivity: 'CHURCH')
  ];

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  Future<ApiResponse<List<DiaCronograma>>> findAll(int scheduleCod) {
    // TODO: implement findAll
    return Future.value(ApiResponse.completed(diasCronograma));
  }

  @override
  Future<void> reorderDays(int from, int to) {
    // TODO: implement reorderDays in api service
    return Future.delayed(Duration(milliseconds: 300));
  }

  @override
  Future<int> createDiaCronograma(int scheduleCod, int dia) {
    // TODO: implement reorderDays in api service
    return Future.value(dia);
  }

  @override
  Future<void> deleteScheduleDay(int scheduleDayCod) {
    // TODO: implement deleteScheduleDay
    return Future.delayed(Duration(milliseconds: 300));
  }
}
