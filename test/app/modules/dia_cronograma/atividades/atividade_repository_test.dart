import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividade_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/interfaces/atividade_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

class MockClient extends Mock implements Dio {}

void main() {
  IAtividadeRepository repository;

  setUp(() {
    repository = AtividadeRepository();
  });

  group('AtividadeRepository Test', () {
     test("First Test", () {
       expect(repository, isInstanceOf<AtividadeRepository>());
     });

    test('find all atividades by idDiaCronograma', () async {
    ApiResponse<List<Atividade>> atividades = await repository.findAll(1);
    expect(atividades.data.isEmpty, false);
    });
  });
}
