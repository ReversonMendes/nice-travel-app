import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_details.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';

class DayScheduleList extends StatefulWidget {
  final Cronograma _cronograma;

  DayScheduleList(this._cronograma);

  @override
  _DayScheduleListState createState() => _DayScheduleListState(_cronograma);
}

class _DayScheduleListState extends State<DayScheduleList> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();

  final Cronograma _cronograma;

  _DayScheduleListState(this._cronograma);

  @override
  Widget build(BuildContext context) {
    return APIList<ApiResponse<List<DiaCronograma>>>(
        elements: _controller.findAll(_cronograma.scheduleCod),
        onCompleted: (list) => _controller.fillDiasCronogramas(list),
        listWidget:
            Observer(builder: (_) => buildList(_controller.diasCronograma)));
  }

  Widget buildList(List<DiaCronograma> diasCronograma) {
    return Expanded(
      child: ReorderableListView(
          children: [
            for (var item in diasCronograma)
              Container(
                key: Key("day_details_${item.day}"),
                height: 100,
                child: DayScheduleDetails(
                  item,
                  _cronograma,
                ),
              )
          ],
          onReorder: (from, to) {
            if (to == diasCronograma.length) {
              to = diasCronograma.length - 1;
            }
            var idFrom = diasCronograma[from].id;
            var idTo = diasCronograma[to].id;
            _controller.reorderDays(idFrom, idTo);
          }),
    );
  }
}
