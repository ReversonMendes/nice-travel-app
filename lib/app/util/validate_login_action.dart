import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/util/show_toast.dart';

validateLoginAction(
    {@required BuildContext context,
    @required SessionUser user,
    @required Cronograma cronograma,
    @required Function successAction,
    @required Function duplicationAction}) {
  if (user == null) {
    buildShowSnackBarToLogin(context);
  } else {
    if (cronograma != null && !_isOwner(user, cronograma)) {
      buildShowSnackBarToDuplicateSchedule(
          context, user, cronograma, duplicationAction);
    } else {
      Function.apply(successAction, []);
    }
  }
}

bool _isOwner(SessionUser user, Cronograma cronograma) =>
    cronograma.scheduleCod != null &&
    user != null &&
    user.uid == cronograma.userUID;
