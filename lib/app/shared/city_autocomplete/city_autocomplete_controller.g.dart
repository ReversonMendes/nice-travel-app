// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_autocomplete_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CityAutocompleteController on _CityAutocompleteControllerBase, Store {
  final _$_placeIdAtom = Atom(name: '_CityAutocompleteControllerBase._placeId');

  @override
  String get _placeId {
    _$_placeIdAtom.reportRead();
    return super._placeId;
  }

  @override
  set _placeId(String value) {
    _$_placeIdAtom.reportWrite(value, super._placeId, () {
      super._placeId = value;
    });
  }

  final _$_nameCityAtom =
      Atom(name: '_CityAutocompleteControllerBase._nameCity');

  @override
  String get _nameCity {
    _$_nameCityAtom.reportRead();
    return super._nameCity;
  }

  @override
  set _nameCity(String value) {
    _$_nameCityAtom.reportWrite(value, super._nameCity, () {
      super._nameCity = value;
    });
  }

  final _$_CityAutocompleteControllerBaseActionController =
      ActionController(name: '_CityAutocompleteControllerBase');

  @override
  void changePlaceId(String placeId) {
    final _$actionInfo = _$_CityAutocompleteControllerBaseActionController
        .startAction(name: '_CityAutocompleteControllerBase.changePlaceId');
    try {
      return super.changePlaceId(placeId);
    } finally {
      _$_CityAutocompleteControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeNameCity(String nameCity) {
    final _$actionInfo = _$_CityAutocompleteControllerBaseActionController
        .startAction(name: '_CityAutocompleteControllerBase.changeNameCity');
    try {
      return super.changeNameCity(nameCity);
    } finally {
      _$_CityAutocompleteControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic clearPlaceID() {
    final _$actionInfo = _$_CityAutocompleteControllerBaseActionController
        .startAction(name: '_CityAutocompleteControllerBase.clearPlaceID');
    try {
      return super.clearPlaceID();
    } finally {
      _$_CityAutocompleteControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
