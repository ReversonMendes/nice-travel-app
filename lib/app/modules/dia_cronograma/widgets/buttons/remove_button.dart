import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';

class RemoveButton extends StatefulWidget {
  @override
  _RemoveButtonState createState() => _RemoveButtonState();
}

class _RemoveButtonState extends State<RemoveButton> {

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      key: Key('remover_schedule'),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.delete,
            size: 40.0,
            color: Colors.red,
          ),
          textButton("Remover"),
        ],
      ),
      onPressed: () {
        removeDialog(context, "Deseja remover esse cronograma?",
            () => deleteAction(context));
      },
    );
  }

  deleteAction(BuildContext context) {
    showCircularProgress(context);
    //Todo deletar o schedule
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        textAlign: TextAlign.center,
      );
}
