import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/home/home_page.dart';
import 'package:nice_travel/app/shared/widgets/navigation/navigation_drawer.dart';

void main() {
  initModules([AppModule(), HomeModule()]);
  testWidgets('HomePage has title', (tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage(title: 'Home')));
    final titleFinder = find.text('Home');
    expect(titleFinder, findsOneWidget);
  });

  testWidgets('HomePage has drawer', (tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage()));
    final drawerFinder = find.byIcon(Icons.menu);
    expect(drawerFinder, findsOneWidget);
    await tester.tap(drawerFinder);
    await tester.pump();
    final navigationDrawerFinder = find.byType(NavigationDrawer);
    expect(navigationDrawerFinder, findsOneWidget);
  });
}
