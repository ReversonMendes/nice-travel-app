import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';

import 'buttons/add_button.dart';
import 'buttons/publish_button.dart';
import 'buttons/remove_button.dart';
import 'buttons/vote_button.dart';

class ButtonBarScheduleDay extends StatelessWidget {
  final Cronograma _cronograma;

  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  ButtonBarScheduleDay(this._cronograma);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buildQtdStar(context),
        Wrap(
          children: <Widget>[
            Visibility(
                visible: !isOwner(),
                child: VoteButton(
                    sessionUser: _securityService.getSessionUser(),
                    cronograma: _cronograma)),
            Visibility(visible: isOwner(), child: RemoveButton()),
            Visibility(
                visible: isOwner() && !_cronograma.isPublish,
                child: PublishButton()),
            Visibility(
                child: AddButton(
                    sessionUser: _securityService.getSessionUser(),
                    cronograma: _cronograma)),
          ],
        ),
      ],
    );
  }

  Widget buildQtdStar(BuildContext context) {
    return Text(
      '${_cronograma.numberStar} ⭐',
      style: TextStyle(
          color: Colors.black,
          fontFamily: "OpenSans",
          fontSize: 24,
          fontWeight: FontWeight.bold),
    );
  }

  bool isOwner() =>
      _cronograma.scheduleCod != null &&
      _securityService.isLoggedIn() &&
      _securityService.getSessionUser().uid == _cronograma.userUID;
}
