import 'package:flutter/material.dart';

class LoadingUtil {

  static void showLoadingAlert(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
              Container(
                  margin: EdgeInsets.only(left: 8.0),
                  child: new Text(
                    'Carregando...',
                    style: TextStyle(color: Colors.white),
                  )),
            ],
          ),
        );
      },
    );
  }
}
