import 'package:intl/intl.dart';

final formatter = NumberFormat.currency(locale: "pt_BR", symbol: "");
final dateFormat = DateFormat("dd/MM/yyyy");
final timeFormat = DateFormat("HH:mm");

String formatMoney(valorAPagar) =>
    valorAPagar == null ? null : formatter.format(valorAPagar);

int diffDaysToNow(dt) {
  return dt == null ? 0 : dt.difference(DateTime.now()).inDays;
}

String formatDate(date) {
  return dateFormat.format(date);
}

String formatHoraToString(date) {
  return timeFormat.format(date);
}

DateTime formatStringToHora(String date) {
  return timeFormat.parse(date);
}
