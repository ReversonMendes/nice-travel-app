import 'dart:developer';

import 'package:dio/native_imp.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'model/places_model.dart';

class GooglePlacesAPI implements ICityAutocomplete {
  final DioForNative client;

  GooglePlacesAPI(this.client);

  GooglePlacesModel googlePlacesModel = new GooglePlacesModel();

  Future<List<GooglePlacesModel>> getCities(String query) async {
    log("Calling Google Place API with Query: " + query);
    var response = await client.get(
        "https://maps.googleapis.com/maps/api/place/autocomplete/json",
        queryParameters: {
          "input": query,
          "language": "pt-BR",
          "types": "(cities)",
          //TODO include sessiontoken=1234567890
          "key": FlavorConfig.instance.variables["google_place_api"]
        });
    List<GooglePlacesModel> lista =
        googlePlacesModel.fromJsonList(response.data['predictions']);
    return lista;
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }
}
