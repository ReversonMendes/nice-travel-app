import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';

part 'dia_cronograma.g.dart';

class DiaCronograma = DiaCronogramaBase with _$DiaCronograma;

abstract class DiaCronogramaBase with Store {
  int day;

  @observable
  double priceDay;

  int id;
  int qtdActivities;
  String typeFirstActivity;

  DiaCronogramaBase(
      {@required this.day,
      @required this.priceDay,
      @required this.id,
      @required this.qtdActivities,
      @required this.typeFirstActivity});

  DiaCronogramaBase.newInstance(int day) {
    this.qtdActivities = 0;
    this.priceDay = 0.0;
    this.day = day;
  }

  @action
  setPrice(double price){
    this.priceDay = price;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaCronogramaBase &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
