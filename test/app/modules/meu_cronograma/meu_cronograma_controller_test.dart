import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_controller.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';

void main() {
  initModule(MeuCronogramaModule());
  MeuCronogramaController meuCronograma;

  setUp(() {
    meuCronograma = MeuCronogramaModule.to.get<MeuCronogramaController>();
  });

  group('MeuCronogramaController Test', () {
      test("First Test", () {
        expect(meuCronograma, isInstanceOf<MeuCronogramaController>());
      });

  });
}
