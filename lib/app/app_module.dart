import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_widget.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/shared/city_autocomplete/google_places_repository.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'app_controller.dart';
import 'security_service.dart';

class AppModule extends MainModule {

  static const MEUS_CRONOGRAMAS_PATH = '/meus-cronogramas';
  static const VIAGEM_PATH = '/viagens';
  static const DIA_CRONOGRAMA_PATH = '/dia-cronograma';

  @override
  List<Bind> get binds => [
        Bind<Dio>((i) => DioForNative()),
        Bind<ICityAutocomplete>((i) => GooglePlacesAPI(i<Dio>())),
        $SecurityService,
        $AppController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HomeModule()),
        ModularRouter(MEUS_CRONOGRAMAS_PATH, module: MeuCronogramaModule()),
        ModularRouter(DIA_CRONOGRAMA_PATH, module: DiaCronogramaModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
