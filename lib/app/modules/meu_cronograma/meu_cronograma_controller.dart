import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import 'interfaces/meu_cronograma_repository_interface.dart';

part 'meu_cronograma_controller.g.dart';

@Injectable()
class MeuCronogramaController = _MeuCronogramaControllerBase
    with _$MeuCronogramaController;

abstract class _MeuCronogramaControllerBase with Store {

  IMeuCronogramaRepository _repository =
      MeuCronogramaModule.to.get<IMeuCronogramaRepository>();

  Future<ApiResponse<List<Cronograma>>> findAll() {
    return _repository.findAll();
  }
}
