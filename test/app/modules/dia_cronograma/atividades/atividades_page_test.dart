import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_page.dart';

void main() {
  initModules([AppModule(), AtividadesModule()]);

  testWidgets('AtividadesPage has Scaffol', (tester) async {
    var diaCronograma = DiaCronograma(
        day: 1,
        id: 1,
        priceDay: 25,
        qtdActivities: 5,
        typeFirstActivity: 'Park');
    await tester.pumpWidget(buildTestableWidget(AtividadesPage(
      cronograma: Cronograma(),
      diaCronograma: diaCronograma,
    )));
    final scaffoldFinder = find.byType(Scaffold);
    expect(scaffoldFinder, findsOneWidget);
    final appBarFinder = find.byType(AppBar);
    expect(appBarFinder, findsOneWidget);
  });
}
