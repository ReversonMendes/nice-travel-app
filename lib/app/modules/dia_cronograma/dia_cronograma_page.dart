import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/button_bar_schedule_day.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_list.dart';
import 'package:nice_travel/app/shared/custom_box_shadow.dart';

class DiaCronogramaPage extends StatelessWidget {
  final Cronograma cronograma;
  final double _heightAppBar = 240;

  const DiaCronogramaPage({Key key, @required this.cronograma})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                imageAppBar(context),
                cityCardContainer(context),
              ],
            ),
          ],
        ),
      ),
    ));
  }

  Widget cityCardContainer(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - _heightAppBar,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: _heightAppBar),
      child: Container(
        child: Column(
          children: <Widget>[
            cityInfoWidget(),
            const SizedBox(
              height: 8.0,
            ),
            DayScheduleList(cronograma),
          ],
        ),
      ),
    );
  }

  Widget imageAppBar(BuildContext context) {
    CarouselOptions options = CarouselOptions(
      height: 240,
      viewportFraction: 0.8,
      autoPlay: true,
      enlargeCenterPage: true,
    );
    return CarouselSlider.builder(
      itemCount: cronograma.imagesUrl.length,
      options: options,
      itemBuilder: (BuildContext context, int itemIndex) => Container(
          margin: EdgeInsets.symmetric(horizontal: 3.0),
          height: _heightAppBar,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AppBar(
                leading: new IconButton(
                  key: Key("schedule_back_button_$itemIndex"),
                  icon: new Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                backgroundColor: Colors.transparent,
                elevation: 0,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      cityNameTitle(),
                    ]),
              ),
            ],
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [buildBoxShadow(3.0)],
            image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(1), BlendMode.dstATop),
                image: CachedNetworkImageProvider(
                    cronograma.imagesUrl[itemIndex])),
          )),
    );
  }

  Widget cityInfoWidget() {
    return Container(
        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [buildBoxShadow(3.0)],
        ),
        child: ButtonBarScheduleDay(cronograma));
  }

  Widget cityNameTitle() {
    return Text(
      '${cronograma.cityAddress}',
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: Colors.white,
        fontFamily: "Literata",
        fontWeight: FontWeight.bold,
        fontSize: 26,
      ),
    );
  }
}
