
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';


abstract class IMeuCronogramaRepository implements Disposable {

  Future<ApiResponse<List<Cronograma>>> findAll();
  Future<void> save(MeuCronograma meuCronograma);
  Future<void> remove(MeuCronograma meuCronograma);
}
