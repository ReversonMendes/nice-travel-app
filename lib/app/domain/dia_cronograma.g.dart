// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dia_cronograma.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DiaCronograma on DiaCronogramaBase, Store {
  final _$priceDayAtom = Atom(name: 'DiaCronogramaBase.priceDay');

  @override
  double get priceDay {
    _$priceDayAtom.reportRead();
    return super.priceDay;
  }

  @override
  set priceDay(double value) {
    _$priceDayAtom.reportWrite(value, super.priceDay, () {
      super.priceDay = value;
    });
  }

  final _$DiaCronogramaBaseActionController =
      ActionController(name: 'DiaCronogramaBase');

  @override
  dynamic setPrice(double price) {
    final _$actionInfo = _$DiaCronogramaBaseActionController.startAction(
        name: 'DiaCronogramaBase.setPrice');
    try {
      return super.setPrice(price);
    } finally {
      _$DiaCronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
priceDay: ${priceDay}
    ''';
  }
}
