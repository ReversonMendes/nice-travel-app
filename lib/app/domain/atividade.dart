import 'package:mobx/mobx.dart';

part 'atividade.g.dart';

class Atividade = BaseAtividade with _$Atividade;

abstract class BaseAtividade  with Store {

  @observable
  String nameOfPlace;

  String description;
  double price;
  String styleActivity;
  DateTime startActivityDate;
  DateTime finishActivityDate;
  int idScheduleDay;
  int id;

  BaseAtividade(
      {this.description,
      this.nameOfPlace,
      this.price,
      this.styleActivity,
      this.startActivityDate,
      this.finishActivityDate,
      this.idScheduleDay,
      this.id});

  BaseAtividade.newInstance(int idScheduleDay) {
    this.idScheduleDay = idScheduleDay;
    this.price = 0.0;
    this.styleActivity = "OTHER";
    this.startActivityDate = DateTime(1, 1, 2000);
    this.finishActivityDate = DateTime(1, 1, 2000);
    this.description = '';
    this.nameOfPlace = '';
  }

  setDescription(String description) {
    this.description = description;
  }

  @action
  setNameOfPlace(String nameOfPlace) {
    this.nameOfPlace = nameOfPlace;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Atividade && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
