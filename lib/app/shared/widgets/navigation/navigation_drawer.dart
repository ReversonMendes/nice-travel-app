import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/shared/widgets/navigation/tab_model.dart';
import 'package:nice_travel/app/util/nice_travel_icons.dart';

import 'drawer_tile.dart';

class NavigationDrawer extends StatelessWidget {
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  Widget _buildDecoration() => Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color.fromARGB(255, 203, 236, 241), Colors.white],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
      );

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Stack(
      children: <Widget>[
        _buildDecoration(),
        ListView(
          padding: EdgeInsets.only(left: 32.0, top: 16.0),
          children: <Widget>[
            _buildLogo(context),
            Divider(height: 20),
            createDrawerTiles(),
          ],
        )
      ],
    ));
  }

  Container _buildLogo(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      height: 170.0,
      child: Stack(
        children: <Widget>[
          Center(
            child: Image.asset("assets/images/logo.png"),
          ),
          buildSignInButton(context)
        ],
      ),
    );
  }

  Positioned buildSignInButton(BuildContext context) {
    return Positioned(
      left: 0.0,
      bottom: 0.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            " ${!_securityService.isLoggedIn() ? "" : "Olá, " + _securityService.getSessionUser().displayName}",
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          GestureDetector(
            child: buildLoginButton(context),
            onTap: () {
              if (!_securityService.isLoggedIn()) {
                /*TODO login*/
              } else {
                _securityService.signOut();
              }
            },
          )
        ],
      ),
    );
  }

  Widget buildLoginButton(BuildContext context) {
    if (!_securityService.isLoggedIn()) {
      return Row(
        children: <Widget>[
          Text(
            "Entrar ",
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
          Icon(
            NiceTravelIcons.login,
            size: 16,
            color: Theme.of(context).primaryColor,
          )
        ],
      );
    }
    return Row(
      children: <Widget>[
        Text(
          "Sair ",
          style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 16.0,
              fontWeight: FontWeight.bold),
        ),
        Icon(
          NiceTravelIcons.logout,
          size: 16,
          color: Theme.of(context).primaryColor,
        )
      ],
    );
  }

  Widget createDrawerTiles() {
    return Column(
      children: TabModel.getTabModels().map((e) => drawerTile(e)).toList(),
    );
  }

  DrawerTile drawerTile(TabModel tab) {
    return DrawerTile(tab.icon, tab.name, tab.navigationPath,
        key: Key("DrawerTile_${tab.navigationPath}"));
  }
}
