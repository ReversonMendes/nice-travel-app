import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/add_button.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  setUpAll(() {
    Modular.init(AppModule());
    initModules([AppModule(), DiaCronogramaModule(), AtividadesModule()]);
  });

  test('Test Get ModularRouter', () {
    expect(Modular.selectRoute(AppModule.DIA_CRONOGRAMA_PATH),
        isA<ModularRouter>());

    expect(
        Modular.selectRoute(AppModule.DIA_CRONOGRAMA_PATH +
            DiaCronogramaModule.ATIVIDADES_PATH),
        isA<ModularRouter>());
  });

  testWidgets('AddButon action', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '123');

    SessionUser sessionUser = SessionUser(
        displayName: 'Thiago',
        email: 'thiago@gmail.com',
        tokenMessage: null,
        uid: "123");

    await tester.pumpWidget(buildTestableWidget(
      Scaffold(
          body: AddButton(
        cronograma: cronograma,
        sessionUser: sessionUser,
      )),
    ));
    final dayScheduleFinder = find.byType(MaterialButton);
    expect(dayScheduleFinder, findsOneWidget);

    await tester.tap(dayScheduleFinder);
    await tester.pump();

    final atividadePageFinder = find.byType(Scaffold);
    expect(atividadePageFinder, findsOneWidget);
  });
}
