import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/interfaces/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_repository.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';


class MockClient extends Mock implements Dio {}

void main() {
  IMeuCronogramaRepository repository;

  setUp(() {
    repository = MeuCronogramaRepository();
  });

  group('MeuCronogramaRepository Test', () {
     test("First Test", () {
       expect(repository, isInstanceOf<MeuCronogramaRepository>());
     });

    test('Test Save Cronograma', () async {
      ApiResponse<List<Cronograma>> cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 1);

      repository.save(MeuCronograma.initValues('_nomeLugar', 2, null));
      cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 2);
    });
  });
}
