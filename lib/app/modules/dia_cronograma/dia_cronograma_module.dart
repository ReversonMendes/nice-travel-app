import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';

import 'dia_cronograma_controller.dart';
import 'dia_cronograma_page.dart';
import 'dia_cronograma_repository.dart';
import 'interfaces/dia_cronograma_repository_interface.dart';

class DiaCronogramaModule extends ChildModule {

  static const ATIVIDADES_PATH = '/atividades';

  @override
  List<Bind> get binds => [
        Bind<IDiaCronogramaRepository>((i) => DiaCronogramaRepository()),
        $DiaCronogramaController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => DiaCronogramaPage(cronograma: args.data)),
    ModularRouter(ATIVIDADES_PATH, module: AtividadesModule()),
      ];

  static Inject get to => Inject<DiaCronogramaModule>.of();
}
