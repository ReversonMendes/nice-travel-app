import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';

void main() {
  setUpAll(() {
    Modular.init(AppModule());
  });

  group("Group router", () {
    test('Test Viagem Path', () {
      expect(Modular.selectRoute(AppModule.VIAGEM_PATH), isA<ModularRouter>());
    });

    test('Test Dia Cronogroma Path', () {
      expect(Modular.selectRoute(AppModule.DIA_CRONOGRAMA_PATH),
          isA<ModularRouter>());
    });

    test('Test Meus Cronogramas Path', () {
      expect(Modular.selectRoute(AppModule.MEUS_CRONOGRAMAS_PATH),
          isA<ModularRouter>());
    });
  });
}
