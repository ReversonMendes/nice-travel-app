import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';

ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
    buildShowSnackBarToLogin(BuildContext context) {
  Scaffold.of(context).removeCurrentSnackBar();
  return Scaffold.of(context).showSnackBar(
    SnackBar(
      content: const Text('É necessário realizar login'),
      action: SnackBarAction(
          label: 'Login', onPressed: () => navigateToSignIn(context)),
    ),
  );
}

void navigateToSignIn(BuildContext context) {
  //TODO go to login page
}

ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
    buildShowSnackBarToDuplicateSchedule(BuildContext context, SessionUser user,
        Cronograma cronograma, Function duplicate) {
  Scaffold.of(context).removeCurrentSnackBar();
  return Scaffold.of(context).showSnackBar(
    SnackBar(
      content:
          const Text('Para alterar é necessário adicionar esse cronograma.'),
      action: SnackBarAction(
          key: Key('adicionar_snackbar'),
          label: 'Adicionar',
          onPressed: () => duplicate),
    ),
  );
}

void showToastMessage(String message, GlobalKey<ScaffoldState> _scaffoldKey) {
  _scaffoldKey.currentState.removeCurrentSnackBar();
  _scaffoldKey.currentState.showSnackBar(
    SnackBar(content: Text(message)),
  );
}