import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/nova_atividade/nova_atividade_page.dart';

void main() {
  initModules([AppModule(), AtividadesModule()]);

  testWidgets('NovaAtividadePage has formFinder', (tester) async {
    var diaCronograma = DiaCronograma(
        day: 1,
        id: 1,
        priceDay: 25,
        qtdActivities: 5,
        typeFirstActivity: 'Park');
    var atividade = Atividade(
        id: 2,
        nameOfPlace: 'Almoço no Pirão Baiano',
        price: 45.0,
        styleActivity: 'RESTAURANT');
    await tester.pumpWidget(buildTestableWidget(NovaAtividadePage(
      atividade: atividade,
      cronograma: Cronograma(),
      diaCronograma: diaCronograma,
    )));
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);
  });
}
