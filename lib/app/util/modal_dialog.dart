import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

void removeDialog(BuildContext context, String title, Function onDelete) {
  showDialog(
      context: context,
      builder: (_) => AssetGiffyDialog(
            image: Image.asset("assets/images/noo.gif"),
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'Essa ação não poderá ser revertida.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            entryAnimation: EntryAnimation.BOTTOM_LEFT,
            onOkButtonPressed: () => Function.apply(onDelete, []),
          ));
}

void publishCronogramaDialog(BuildContext context, String title, Function onPublish) {
  showDialog(
      context: context,
      builder: (_) => AssetGiffyDialog(
            image: Image.asset("assets/images/yes.gif"),
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'Não será possível possível despublicar.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            entryAnimation: EntryAnimation.BOTTOM_LEFT,
            onOkButtonPressed: () => Function.apply(onPublish, []),
          ));
}
