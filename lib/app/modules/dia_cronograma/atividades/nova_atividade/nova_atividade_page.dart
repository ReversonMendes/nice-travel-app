import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:intl/intl.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/nova_atividade/delete_atividade_button.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/icon_style_activity.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/show_toast.dart';

class NovaAtividadePage extends StatefulWidget {
  final Atividade atividade;
  final DiaCronograma diaCronograma;
  final Cronograma cronograma;

  const NovaAtividadePage(
      {Key key,
      @required this.atividade,
      @required this.diaCronograma,
      @required this.cronograma})
      : super(key: key);

  @override
  _NovaAtividadePageState createState() =>
      _NovaAtividadePageState(atividade, diaCronograma, cronograma);
}

class _NovaAtividadePageState extends State<NovaAtividadePage> {
  final AtividadesController controller =
      AtividadesModule.to.get<AtividadesController>();

  final Atividade atividade;
  final double currentAtividadePrice;
  final DiaCronograma diaCronograma;
  final Cronograma cronograma;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  _NovaAtividadePageState(this.atividade, this.diaCronograma, this.cronograma)
      : currentAtividadePrice = atividade.price;

  MoneyMaskedTextController _moneyController;

  updatePrice() {
    atividade.price = _moneyController.numberValue;
  }

  @override
  void initState() {
    super.initState();
    initControllers();
  }

  void initControllers() {
    TextEditingController(text: atividade.description);
    _moneyController = MoneyMaskedTextController(
        initialValue: atividade.price,
        decimalSeparator: ',',
        thousandSeparator: '.');
    _moneyController.addListener(updatePrice);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.green,
        elevation: 0,
        title: Observer(
          builder: (_) => Text(
            atividade.nameOfPlace ?? '',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        leading: CloseButton(),
      ),
      body: builtActivity(),
    );
  }

  builtActivity() {
    return SingleChildScrollView(
      key: Key("new_acitivy_scroll"),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Form(
              autovalidateMode: AutovalidateMode.always,
              key: _formKey,
              child: Wrap(
                runSpacing: 20,
                runAlignment: WrapAlignment.start,
                children: <Widget>[
                  _builtNameText(),
                  Row(children: [
                    _builtPriceText(),
                    SizedBox(width: 10),
                    _builtStyleActivity(),
                  ]),
                  Row(children: [
                    _builtTimeInicio(),
                    SizedBox(width: 10),
                    _builtTimeFim(),
                  ]),
                  _builtDescriptionText(),
                ],
              ),
            ),
            buildSalvarButton(),
            buildRemoverButton(),
          ],
        ),
      ),
    );
  }

  Widget _builtDescriptionText() {
    return TextFormField(
      key: Key("description_text"),
      initialValue: atividade.description,
      onChanged: atividade.setDescription,
      maxLines: 10,
      decoration: InputDecoration(
          labelText: "Descrição",
          labelStyle: TextStyle(color: Colors.black),
          border: OutlineInputBorder()),
      style: TextStyle(color: Colors.black, fontSize: 18.0),
      textAlign: TextAlign.justify,
    );
  }

  Widget _builtNameText() {
    return TextFormField(
      key: Key("activity_name_text"),
      initialValue: atividade.nameOfPlace,
      onChanged: atividade.setNameOfPlace,
      validator: FormBuilderValidators.required(context,
          errorText: "Campo Obrigatório"),
      decoration: InputDecoration(
          labelText: "Atividade",
          labelStyle: TextStyle(color: Colors.black),
          border: OutlineInputBorder()),
      style: TextStyle(color: Colors.black, fontSize: 18.0),
      textAlign: TextAlign.start,
    );
  }

  _builtPriceText() {
    final double width = MediaQuery.of(context).size.width;
    return SizedBox(
      width: width / 3,
      child: TextFormField(
          validator: FormBuilderValidators.required(context,
              errorText: "Campo Obrigatório"),
          key: Key("price_text"),
          textAlign: TextAlign.justify,
          decoration: InputDecoration(
              labelText: "Preço diário",
              labelStyle: TextStyle(color: Colors.black),
              border: OutlineInputBorder()),
          controller: _moneyController,
          keyboardType: TextInputType.number,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w900,
          )),
    );
  }

  _builtTimeInicio() {
    return Expanded(
      child: DateTimeField(
        key: Key("date_start_text"),
        readOnly: true,
        decoration: InputDecoration(
            labelText: 'Início da atividade',
            labelStyle: TextStyle(color: Colors.black),
            border: OutlineInputBorder()),
        format: DateFormat("HH:mm"),
        onShowPicker: (context, currentValue) async {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(
                currentValue ?? atividade.startActivityDate),
          );
          return DateTimeField.convert(time);
        },
        initialValue: atividade.startActivityDate,
        onChanged: (dt) => setState(() => atividade.startActivityDate = dt),
      ),
    );
  }

  _builtTimeFim() {
    return Expanded(
      child: DateTimeField(
        key: Key("date_end_text"),
        readOnly: true,
        decoration: InputDecoration(
            labelText: 'Fim da atividade',
            labelStyle: TextStyle(color: Colors.black),
            border: OutlineInputBorder()),
        format: DateFormat("HH:mm"),
        onShowPicker: (context, currentValue) async {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(
                currentValue ?? atividade.finishActivityDate),
          );
          return DateTimeField.convert(time);
        },
        validator: _validateDate,
        initialValue: atividade.finishActivityDate,
        onChanged: (dt) => setState(() => atividade.finishActivityDate = dt),
      ),
    );
  }

  String _validateDate(DateTime date) {
    if (atividade.finishActivityDate != null &&
        atividade.startActivityDate
                .difference(atividade.finishActivityDate)
                .inMinutes >
            0) {
      return "A data final precisa ser maior do que a data inicial.";
    }
    return null;
  }

  _builtStyleActivity() {
    return Expanded(
      child: FormBuilderDropdown(
        name: "atividade",
        decoration: InputDecoration(
            labelText: 'Tipo da atividade',
            labelStyle: TextStyle(color: Colors.black),
            border: OutlineInputBorder()),
        initialValue: atividade.styleActivity.toUpperCase(),
        hint: Text('Select Gender'),
        onChanged: (tp) => atividade.styleActivity = tp,
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(context,
              errorText: "Campo Obrigatório")
        ]),
        items: getStyleDescription()
            .map((tp) => DropdownMenuItem(
                value: tp.styleLabel,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    IconStyleActivity(tp.styleLabel).icon,
                    Text("${tp.portugueseLabel}"),
                  ],
                )))
            .toList(),
      ),
    );
  }

  buildSalvarButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: MaterialButton(
        key: Key('saveatividade_button'),
        height: 45,
        //Wrap with Material
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
        elevation: 18.0,
        color: Theme.of(context).primaryColor,
        clipBehavior: Clip.antiAlias,
        // Add This
        child: new Text('Salvar',
            style: new TextStyle(fontSize: 16.0, color: Colors.white)),
        onPressed: () {
          save(context);
        },
      ),
    );
  }

  save(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      showCircularProgress(context);
      controller.saveAtividade(atividade).then((id) {
        Navigator.of(context).pop(); //Progress
        Navigator.of(context).pop(); //Page
        _updateDiaCronogramaPrice();
        controller.saveLocal(atividade, id);
      });
    } else {
      showToastMessage("É necessário preencher todos os campos", _scaffoldKey);
    }
  }

  void _updateDiaCronogramaPrice() {
    diaCronograma.setPrice(
        diaCronograma.priceDay - currentAtividadePrice + atividade.price);
  }

  buildRemoverButton() {
    return Visibility(
      visible: atividade.id != null,
      child: DeleteButton(
        atividade: atividade,
      ),
    );
  }
}
