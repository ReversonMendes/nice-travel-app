import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/widgets/atividade_card.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/widgets/new_atividade_button.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/icon_style_activity.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

import 'atividades_controller.dart';

class AtividadesPage extends StatefulWidget {
  final DiaCronograma diaCronograma;
  final Cronograma cronograma;

  const AtividadesPage(
      {Key key, @required this.diaCronograma, @required this.cronograma})
      : super(key: key);

  @override
  _AtividadesPageState createState() => _AtividadesPageState();
}

class _AtividadesPageState
    extends ModularState<AtividadesPage, AtividadesController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: APIList<ApiResponse<List<Atividade>>>(
          elements: controller.findAll(widget.diaCronograma.id),
          onCompleted: (list) => controller.fillAtividades(list),
          listWidget:
              Observer(builder: (_) => buildList(controller.atividades))),
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      title: Row(children: [
        new Text('${widget.diaCronograma.day}º Dia '),
      ]),
      actions: <Widget>[
        NewAtividadeButton(widget.cronograma, widget.diaCronograma)
      ],
    );
  }

  StatelessWidget buildList(List<Atividade> atividades) {
    if (atividades.length > 0) {
      return Timeline.builder(
        position: TimelinePosition.Left,
        itemBuilder: (ctx, i) => createTimeLine(ctx, atividades[i]),
        itemCount: atividades.length,
      );
    } else {
      return new Container(
        child: Center(
            child: Text(
          "Nenhuma atividade cadastrada ainda :(",
          style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        )),
      );
    }
  }

  TimelineModel createTimeLine(BuildContext context, Atividade atividade) {
    return new TimelineModel(
        AtividadeCard(
          cronograma: widget.cronograma,
          diaCronograma: widget.diaCronograma,
          atividade: atividade,
        ),
        iconBackground: IconStyleActivity(atividade.styleActivity).color,
        icon: IconStyleActivity(atividade.styleActivity).icon);
  }
}
