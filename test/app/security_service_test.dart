import 'package:flutter_test/flutter_test.dart';

import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/security_service.dart';

void main() {
  ISecurityService service;

  setUp(() {
   service = SecurityService();
  });

  group('SecurityService Test', () {
   test("First Test", () {
     expect(service, isInstanceOf<ISecurityService>());
   });

  });
}
