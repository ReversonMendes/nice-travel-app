import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/util/format_util.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class AtividadeCard extends StatelessWidget {
  final Cronograma cronograma;
  final DiaCronograma diaCronograma;
  final Atividade atividade;

  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  AtividadeCard(
      {Key key,
      @required this.cronograma,
      @required this.diaCronograma,
      @required this.atividade})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: () => validateLoginAction(
          context: context,
          user: _securityService.getSessionUser(),
          cronograma: cronograma,
          successAction: () => _sendToEditAtividade(context),
          duplicationAction: () => _duplicate(context, cronograma)),
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 16.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                  "${formatHoraToString(atividade.startActivityDate)} - "
                  "${formatHoraToString(atividade.finishActivityDate)}",
                  style: textTheme.caption),
              Flexible(
                child: Text(
                  atividade.nameOfPlace,
                  style: textTheme.headline6,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Flexible(
                child: Text(
                  'R\$: ${formatMoney(atividade.price)}',
                  style: TextStyle(color: Colors.green, fontSize: 13),
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _duplicate(BuildContext context, Cronograma cronograma) {
    showCircularProgress(context);
    //Todo duplicar schedule
  }

  _sendToEditAtividade(BuildContext context) {
    Map<String, dynamic> parameters = {
      'diaCronograma': diaCronograma,
      'cronograma': cronograma,
      'atividade': atividade
    };

    Navigator.of(context)
        .pushNamed(AtividadesModule.ATIVIDADE_FULL_PATH, arguments: parameters);
  }
}
