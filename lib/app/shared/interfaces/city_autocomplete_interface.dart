import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/shared/city_autocomplete/model/places_model.dart';

abstract class ICityAutocomplete implements Disposable {
  Future<List<GooglePlacesModel>> getCities(String query);
}
