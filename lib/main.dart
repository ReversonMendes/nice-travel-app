import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';

void main() {
  FlavorConfig(
    name: "DEVELOP",
    color: Colors.red,
    location: BannerLocation.bottomStart,
    variables: {
      "google_place_api": 'teste',
    },
  );
  return runApp(ModularApp(module: AppModule()));
}
