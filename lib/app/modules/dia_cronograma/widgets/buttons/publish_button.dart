import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';

class PublishButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      key: Key("publish_schedule_button"),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.publish,
            size: 40.0,
            color: Colors.green,
          ),
          textButton("Publicar"),
        ],
      ),
      onPressed: () {
        publishCronogramaDialog(
            context,
            "Deseja tornar esse cronograma público?",
            () => publishAction(context));
      },
    );
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        textAlign: TextAlign.center,
      );

  publishAction(BuildContext context) {
    //TODO publicar
  }
}
