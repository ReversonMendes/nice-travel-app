import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/atividades/atividades_module.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class NewAtividadeButton extends StatelessWidget {
  final Cronograma cronograma;
  final DiaCronograma diaCronograma;

  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  NewAtividadeButton(this.cronograma, this.diaCronograma);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => validateLoginAction(
          context: context,
          user: _securityService.getSessionUser(),
          cronograma: cronograma,
          successAction: () => _sendToNovaAtividade(context),
          duplicationAction: () => _duplicate(context, cronograma)),
      icon: Icon(Icons.add_circle_outlined),
    );
  }

  void _duplicate(BuildContext context, Cronograma cronograma) {
    showCircularProgress(context);
    //Todo duplicar schedule
  }

  _sendToNovaAtividade(BuildContext context) {
    Map<String, dynamic> parameters = {
      'diaCronograma': diaCronograma,
      'cronograma': cronograma,
      'atividade': Atividade.newInstance(diaCronograma.id)
    };

    Navigator.of(context)
        .pushNamed(AtividadesModule.ATIVIDADE_FULL_PATH, arguments: parameters);
  }
}
